import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class shw1_4 {
    @Test
    void test() {

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 30);
        webDriver.get("https://mail.ru/");
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")).sendKeys("vasifselenium@mail.ru");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                "div.password-input-container.svelte-1eyrl7y > input")).sendKeys("ibaQa12345");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                "div.password-input-container.svelte-1eyrl7y > input")));
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")).click();
        webDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > div.application-mail__overlay > div > div.application-mail__layout.application-mail__layout_main > span > div.layout__column.layout__column_left > " +
                "div.layout__column-wrapper > div > div > div > div:nth-child(1) > div > div > a > span")).click();
        webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.head_container--3W05z > div > div > div.wrap--2sfxq > div > div.contacts--1ofjA > " +
                "div > div > label > div > div > input")).sendKeys("vasifselenium@mail.ru");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.head_container--3W05z > div > div > div.wrap--2sfxq > div > div.contacts--1ofjA > " +
                "div > div > label > div > div > input")));
        webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.subject__container--HWnat > " +
                "div.subject__wrapper--2mk6m > div.container--3QXHv > div > input")).sendKeys("homework1.4");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.subject__container--HWnat > " +
                "div.subject__wrapper--2mk6m > div.container--3QXHv > div > input")));


        Path resourceDirectory = Paths.get("src", "test");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        String fayl = absolutePath + "\\resources\\gratest.txt";

        WebElement basma = webDriver.findElement(By.className("desktopInput--3cWPE"));
        basma.sendKeys(fayl);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__footer > div.compose-app__buttons > span.button2.button2_base.button2_primary.button2_hover-support.js-shortcut > " +
                "span > span")).click();


        WebElement ExpectedResult = webDriver.findElement(By.cssSelector("body > div:nth-child(28) > div > div > div.layer-window__container > div.layer-window__block > div > div > div.layer__header > a"));
        WebElement ActualResult = webDriver.findElement(By.cssSelector("body > div:nth-child(28) > div > div > div.layer-window__container > div.layer-window__block > div > div > div.layer__header > a"));
        Assertions.assertEquals(ExpectedResult, ActualResult);

    }
}
