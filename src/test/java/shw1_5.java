import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.html.HTMLDListElement;

import java.util.List;

public class shw1_5 {
    @Test
    void test() throws InterruptedException {

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 60);
        webDriver.get("https://www.etsy.com/");
        Actions action = new Actions(webDriver);
        WebElement sub1 = webDriver.findElement(By.cssSelector("#catnav-primary-link-10923"));
        Thread.sleep(1000);
        action.moveToElement(sub1).build().perform();
        WebElement sub2 = webDriver.findElement(By.cssSelector("#side-nav-category-link-10936"));
        Thread.sleep(1000);
        action.moveToElement(sub2).build().perform();
        Thread.sleep(1000);
        webDriver.findElement(By.cssSelector("#catnav-l4-11109")).click();
        Thread.sleep(1000);
        webDriver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div" +
                " > a > label")).click();
        Thread.sleep(1000);
        WebElement text = webDriver.findElement(By.cssSelector("#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl" +
                "-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > " +
                "div > div.wt-mt-xs-2.wt-pl-xs-1.wt-pl-md-4.wt-pl-lg-6.wt-pr-xs-1.wt-pr-m" +
                "d-4.wt-pr-lg-6 > div > span > span > " +
                "span:nth-child(2)"));
        String Txt = text.getText();
        System.out.println("ON SALE PRODUCTS NUMBER:" + Txt);

        WebElement ExpectedResult = webDriver.findElement(By.cssSelector("#content > div > div.wt-bg-white.wt-grid__item-md" +
                "-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-pl-xs-1.wt-" +
                "pl-md-4.wt-pl-lg-6.wt-pr-xs-1.wt-pr-md-4.wt-pr-lg-6 > div > span > span > " +
                "span:nth-child(2)"));
        WebElement ActualResult = webDriver.findElement(By.cssSelector("#content > div > div.wt-bg-white.wt-grid__item-md" +
                "-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-pl-xs-1.wt-" +
                "pl-md-4.wt-pl-lg-6.wt-pr-xs-1.wt-pr-md-4.wt-pr-lg-6 > div > span > span > " +
                "span:nth-child(2)"));
        Assertions.assertEquals(ExpectedResult, ActualResult);


    }

}
