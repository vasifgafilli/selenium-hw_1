import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class shw1_1 {
    @Test
    void test() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 30);
        webDriver.get("https://www.google.com");
        webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > " +
                "div.a4bIc > input")).sendKeys("Selenium");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > " +
                "div.a4bIc > input")));
        webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > " +
                "input.gNO89b")).click();
        webDriver.findElement(By.cssSelector("#rso > div:nth-child(1) > div > div > div.yuRUbf > a > h3")).click();
        webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        WebElement ExpectedResult = webDriver.findElement(By.cssSelector("#header > a.headerLink > img.logo.logo-large"));
        WebElement ActualResult = webDriver.findElement(By.cssSelector("#header > a.headerLink > img.logo.logo-large"));
        Assertions.assertEquals(ExpectedResult, ActualResult);
    }
}
