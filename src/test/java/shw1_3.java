import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class shw1_3 {
    @Test
    void test() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.get("https://mail.ru/");
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")).sendKeys("vasifselenium@mail.ru");
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")).sendKeys("asdfasd");
        webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")).click();


        WebElement ExpectedResult = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y"));
        WebElement ActualResult = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y"));
        Assertions.assertEquals(ExpectedResult, ActualResult);
    }
}
